/*
 * ROS_interface.cpp
 *
 * Provides the ROS interface for the gazebo experiments.  Should simulate
 * experimental setup, as well as motion tracking.  Outputs plane state, as well
 * as swarm positions
 */

// Boost Libraries
#include <boost/bind.hpp>

// Gazebo Libraries
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>

// ROS Libraries
#include <ros/ros.h>
#include <ros/callback_queue.h>
#include <ros/subscribe_options.h>
#include <ros/advertise_options.h>
#include <tasc_ros/MFRMFPNodeInput.h>
#include <tasc_ros/MFRMFPNodeState.h>
#include <tasc_ros/MFRMFPParentState.h>

// Standard Libraries
#include <stdio.h>
#include <vector>
#include <thread>

#ifndef __ROS_INTERFACE_CPP__
#define __ROS_INTERFACE_CPP__

#define PID_KP 1.0
#define PID_KD 0.0
#define PID_KI 0.0

#define GRAV 9.81

namespace gazebo
{
  class ROSInterface : public ModelPlugin
  {
  private:
    physics::ModelPtr _model;
    physics::WorldPtr _world;
    physics::JointPtr _plane_joint;
    std::vector<physics::JointPtr> _rob_joints;
    std::vector<common::PID> _rob_pids;
    int _robs_N;
    int _use_PID;
    event::ConnectionPtr _updateConnection;
    std::unique_ptr<ros::NodeHandle> _rosNode;
    std::vector<ros::Subscriber> _rob_vel_sub;
    std::vector<ros::Publisher> _rob_pos_pub;
    std::vector<ros::Publisher> _rob_vel_pub;
    ros::Publisher _plane_st_pub;
    ros::CallbackQueue _rosQueue;
    std::thread _rosQueueThread;

  public:
    ROSInterface(){}
    ~ROSInterface(){}

    void Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf)
    {
      double pid_kp = PID_KP;
      double pid_kd = PID_KD;
      double pid_ki = PID_KI;
      this->_use_PID = 0;

      // load this model and world from parent
      this->_model = _parent;
      this->_world = this->_model->GetWorld();

      // get number of robots from sdf
      if (_sdf->HasElement("numRobs"))
      {
        this->_robs_N = _sdf->Get<int>("numRobs");
      }
      else
      {
        this->_robs_N = 0;
      }

      // load PID gains from sdf
      if (_sdf->HasElement("pidKP"))
      {
        pid_kp = _sdf->Get<double>("pidKP");
      }
      if (_sdf->HasElement("pidKD"))
      {
        pid_kd = _sdf->Get<double>("pidKD");
      }
      if (_sdf->HasElement("pidKI"))
      {
        pid_ki = _sdf->Get<double>("pidKI");
      }
      if (_sdf->HasElement("usePID"))
      {
        this->_use_PID = 1;
      }

      // load joints and setup velocity controllers
      for (int i = 0; i < this->_robs_N; i++)
      {
        char jointName[32];
        sprintf(jointName, "travel_joint%d", i);
        this->_rob_joints.push_back(this->_model->GetJoint(jointName));
        this->_rob_pids.push_back(common::PID(pid_kp, pid_ki, pid_kd));

        if (this->_use_PID)
        {
          this->_model->GetJointController()->SetVelocityPID(this->_rob_joints[i]->GetScopedName(), this->_rob_pids[i]);
          this->_model->GetJointController()->SetVelocityTarget(this->_rob_joints[i]->GetScopedName(), 0.0);
        }
      }

      this->_plane_joint = this->_model->GetJoint("Plane1D::plane_joint");

      // initilize ROS
      if (!ros::isInitialized())
      {
        int argc = 0;
        char** argv = NULL;
        ros::init(argc, argv, "gazebo_ros_interface", ros::init_options::NoSigintHandler);
      }

      // create ROS node
      this->_rosNode.reset(new ros::NodeHandle("gazebo_ros_interface"));

      // subscribe to velocity topics and set publishers
      for (int i = 0; i < this->_robs_N; i++)
      {
        char topic[32];
        sprintf(topic, "/TASC/Swarm/rob%d/u", i);
        ros::SubscribeOptions so_p = ros::SubscribeOptions::create<tasc_ros::MFRMFPNodeInput>(topic, 1, boost::bind(&ROSInterface::OnRobVelMsg, this, i, _1), ros::VoidPtr(), &this->_rosQueue);
        this->_rob_vel_sub.push_back(this->_rosNode->subscribe(so_p));

        sprintf(topic, "/TASC/Swarm/rob%d/x", i);
        ros::AdvertiseOptions ao_p = ros::AdvertiseOptions::create<tasc_ros::MFRMFPNodeState>(topic, 1, NULL, NULL, ros::VoidPtr(), &this->_rosQueue);
        this->_rob_pos_pub.push_back(this->_rosNode->advertise(ao_p));

        sprintf(topic, "/TASC/Swarm/rob%d/u_act", i);
        ros::AdvertiseOptions ao_v = ros::AdvertiseOptions::create<tasc_ros::MFRMFPNodeInput>(topic, 1, NULL, NULL, ros::VoidPtr(), &this->_rosQueue);
        this->_rob_vel_pub.push_back(this->_rosNode->advertise(ao_v));
      }

      // set publishers for plane state
      ros::AdvertiseOptions ao = ros::AdvertiseOptions::create<tasc_ros::MFRMFPParentState>("/TASC/Parent/x", 1, NULL, NULL, ros::VoidPtr(), &this->_rosQueue);
      this->_plane_st_pub = this->_rosNode->advertise(ao);

      // spin up queue helper thread
      this->_rosQueueThread = std::thread(std::bind(&ROSInterface::QueueThread, this));

      printf("ROS Interface loaded for %d robots...\n", this->_robs_N);
      printf("Kp, Ki, and Kd gains for robots set to [%f, %f, %f]\n", pid_kp, pid_ki, pid_kd);
      printf("use PID flag set to %d.\n", this->_use_PID);

      // set OnUpdate to be called when necessary
      this->_updateConnection = event::Events::ConnectWorldUpdateBegin(boost::bind(&ROSInterface::OnUpdate, this, _1));
    }

    void Init(){}

    void OnUpdate(const common::UpdateInfo & /*info*/)
    {
      common::Time t = this->_world->GetSimTime();
      tasc_ros::MFRMFPParentState par_st_msg;
      tasc_ros::MFRMFPNodeState rob_st_msg;
      tasc_ros::MFRMFPNodeInput rob_in_msg;

      par_st_msg.theta = this->_plane_joint->GetAngle(0).Radian();
      par_st_msg.omega = this->_plane_joint->GetVelocity(0);
      par_st_msg.t = t.Float();

      this->_plane_st_pub.publish(par_st_msg);

      for (int i = 0; i < this->_robs_N; i++)
      {
        rob_st_msg.pos = this->_rob_joints[i]->GetAngle(0).Radian();
        rob_st_msg.t = t.Float();
        rob_in_msg.vel = this->_rob_joints[i]->GetVelocity(0);
        rob_in_msg.t = t.Float();

        this->_rob_pos_pub[i].publish(rob_st_msg);
        this->_rob_vel_pub[i].publish(rob_in_msg);
      }

      double force = GRAV*0.2835*sin(this->_plane_joint->GetAngle(0).Radian());
      for (int i = 0; i < this->_robs_N; i++)
      {
        this->_rob_joints[i]->SetForce(0, force);
      }
    }

    void OnRobVelMsg(int ind, const tasc_ros::MFRMFPNodeInput::ConstPtr& _msg)
    {
      if (this->_use_PID)
      {
        this->_model->GetJointController()->SetVelocityTarget(this->_rob_joints[ind]->GetScopedName(), _msg->vel);
      }
      else
      {
        this->_rob_joints[ind]->SetVelocity(0, _msg->vel);
      }
    }

    void QueueThread()
    {
      static const double timeout = 0.01;
      while (this->_rosNode->ok())
      {
        this->_rosQueue.callAvailable(ros::WallDuration(timeout));
      }
    }
  };

  GZ_REGISTER_MODEL_PLUGIN(ROSInterface)
}

#endif
