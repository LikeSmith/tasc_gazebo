/*
 * initilizer.cpp
 *
 * This gazebo plugin will allow ros to initilize the the simulation to a given
 * state.
 */

// boost includes
#include <boost/bind.hpp>

// gazebo includes
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <gazebo/math/gzmath.hh>

// ROS includes
#include <ros/ros.h>
#include <ros/callback_queue.h>
#include <ros/subscribe_options.h>
#include <tasc_ros/MFRMFPParentState.h>
#include <tasc_ros/MFRMFPNodeState.h>

// standard includes
#include <stdio.h>
#include <thread>
#include <vector>

#ifndef __INITILIZER_CPP__
#define __INITILIZER_CPP__

namespace gazebo
{
  class Initilizer : public ModelPlugin
  {
  private:
    physics::ModelPtr _model;
    physics::WorldPtr _world;
    physics::JointPtr _plane_joint;
    std::vector<physics::JointPtr> _rob_joints;
    std::unique_ptr<ros::NodeHandle> _ros_node;
    std::vector<ros::Subscriber> _rob_sub;
    ros::Subscriber _plane_sub;
    ros::CallbackQueue _cb_queue;
    std::thread _ros_queue_thread;
    int _num_robs;

  public:
    Initilizer() {}
    ~Initilizer() {}

    void Load(physics::ModelPtr parent, sdf::ElementPtr sdf)
    {
      // save model and world pointers
      this->_model = parent;
      this->_world = this->_model->GetWorld();

      // get number of robots from sdf
      if (sdf->HasElement("numRobs"))
      {
        this->_num_robs = sdf->Get<int>("numRobs");
      }
      else
      {
        this->_num_robs = 0;
      }

      // load joints
      this->_plane_joint = this->_model->GetJoint("Plane1D::plane_joint");

      for (int i = 0; i < this->_num_robs; i++)
      {
        char joint_name[32];
        sprintf(joint_name, "travel_joint%d", i);
        this->_rob_joints.push_back(this->_model->GetJoint(joint_name));
      }

      // initilize ROS
      if (!ros::isInitialized())
      {
        int argc = 0;
        char** argv = NULL;
        ros::init(argc, argv, "gazebo_initilizer", ros::init_options::NoSigintHandler);
      }

      this->_ros_node.reset(new ros::NodeHandle("gazebo_initilizer"));

      // set up subscribers
      ros::SubscribeOptions so = ros::SubscribeOptions::create<tasc_ros::MFRMFPParentState>("/TASC/Parent/x0", 1, boost::bind(&Initilizer::parent_init_cb, this, _1), ros::VoidPtr(), &this->_cb_queue);
      this->_plane_sub = this->_ros_node->subscribe(so);

      for (int i = 0; i <  this->_num_robs; i++)
      {
        char topic[32];
        sprintf(topic, "/TASC/Swarm/rob%d/x0", i);
        ros::SubscribeOptions so = ros::SubscribeOptions::create<tasc_ros::MFRMFPNodeState>(topic, 1, boost::bind(&Initilizer::node_init_cb, this, i, _1), ros::VoidPtr(), &this->_cb_queue);
        this->_rob_sub.push_back(this->_ros_node->subscribe(so));
      }

      // spin up callback queue
      this->_ros_queue_thread = std::thread(std::bind(&Initilizer::queue_thread_cb, this));

      printf("Initilizer plugin started for plane and %d robots.\n", this->_num_robs);
    }

    void Init() {}

    void parent_init_cb(const tasc_ros::MFRMFPParentState::ConstPtr& msg)
    {
      this->_plane_joint->SetPosition(0, msg->theta);
      this->_plane_joint->SetVelocity(0, msg->omega);
      //printf("Parent initilized to [%f, %f]\n", msg->theta, msg->omega);
    }

    void node_init_cb(int ind, const tasc_ros::MFRMFPNodeState::ConstPtr& msg)
    {
      this->_rob_joints[ind]->SetPosition(0, msg->pos);
      //printf("Robot %d initilized to %f\n", ind, msg->pos);
    }

    void queue_thread_cb()
    {
      static const double timeout = 0.01;
      while (this->_ros_node->ok())
      {
        this->_cb_queue.callAvailable(ros::WallDuration(timeout));
      }
    }

  };

  GZ_REGISTER_MODEL_PLUGIN(Initilizer)
}

#endif
