/*
 * system_recorder.cpp
 * Records states of the system into, saves to csv file for future reference
 */

// Boost Libraries
#include <boost/bind.hpp>

// Gazebo Libraries
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>

// Standard Libraries
#include <stdio.h>
#include <vector>
#include <fstream>

#ifndef __SYSTEM_RECORDER_CPP__
#define __SYSTEM_RECORDER_CPP__

namespace gazebo
{
	class SystemRecorder : public ModelPlugin
	{
		private:
		physics::ModelPtr _model;
		physics::WorldPtr _world;
		physics::JointPtr _plane_joint;
		std::vector<physics::JointPtr> _rob_joints;
		event::ConnectionPtr _updateConnection;
		std::ofstream _out_file;
		int _joints_N;

		public:
		SystemRecorder(){}

		~SystemRecorder()
		{
			this->_out_file.close();
		}

		void Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf)
		{
			this->_model = _parent;
			this->_world = this->_model->GetWorld();

			if (_sdf->HasElement("numRobs"))
			{
				this->_joints_N = _sdf->Get<int>("numRobs");
			}
			else
			{
				this->_joints_N = 0;
			}

			for (int i = 0; i < this->_joints_N; i++)
      {
        char jointName[32];
        sprintf(jointName, "travel_joint%d", i);
        this->_rob_joints.push_back(this->_model->GetJoint(jointName));
      }

			this->_plane_joint = this->_model->GetJoint("Plane1D::plane_joint");

			printf("system_recorder loaded for %d robots...\n", this->_joints_N);

			this->_updateConnection = event::Events::ConnectWorldUpdateBegin(boost::bind(&SystemRecorder::OnUpdate, this, _1));
		}

		void Init()
		{
			char file_name[128];
			sprintf(file_name, "SystemState.csv");
			this->_out_file.open(file_name);
		}

		void OnUpdate(const common::UpdateInfo & /*_info*/)
		{
      common::Time t = _world->GetSimTime();
			double plane_pos = this->_plane_joint->GetAngle(0).Radian();
			double plane_vel = this->_plane_joint->GetVelocity(0);
			std::vector<double> poss;
			std::vector<double> vels;

			for (int i = 0; i < this->_joints_N; i++)
			{
				poss.push_back(this->_rob_joints[i]->GetAngle(0).Radian());
				vels.push_back(this->_rob_joints[i]->GetVelocity(0));
			}

			this->_out_file << t.Double() << "," << plane_pos << "," << plane_vel;

			for (int i = 0; i < this->_joints_N; i++)
			{
				this->_out_file << ", " << poss[i] << ", " << vels[i];
			}

			this->_out_file << std::endl;
		}
	};

	GZ_REGISTER_MODEL_PLUGIN(SystemRecorder)
}

#endif
