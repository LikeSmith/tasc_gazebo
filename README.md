# README #

This repository contains the code and files necessary for the simulation of the TASC project in the Smart Systems Lab at the George Washington University.  Currently this consists primarially of the MFRMFP experiment.  This experiment involves a swarm of robots driving on a plane which is allowed to rotate about an axis.  The goal fo these robots is to balance this plane.

### What is this repository for? ###

* Gazebo models for MFRMFP simulation
* Gazebo plugins for interfacing with ROS.
* Controllers may also be implimented directly in Gazebo, but we use ROS so we can replace the Gazebo node with nodes talking to hardware for real experiment

TASC Version: 0.0.2

### How do I get set up? ###

* Use cmake and make to build the plugins in the plugins directory.
* If you need a new world setup, use the gen_exp.py script to generate a new world file.
* If using ros, fire up a roscore before launching gazebo
* launch gazebo with desired world file.
* If you add a new plugin, be sure to add the template for it so teh gen_exp.py scirpt can add it to new experimental setups.

### Who do I talk to? ###

* Kyle Crandall <crandallk@gwu.edu>