#!/bin/sh

echo "Setting up Gazebo environment..."

TASC_GAZEBO_DIR=$(pwd)

if [[ $GAZEBO_MODEL_PATH != *"$TASC_GAZEBO_DIR/models"* ]]; then
	export GAZEBO_MODEL_PATH=$TASC_GAZEBO_DIR/models:$GAZEBO_MODEL_PATH
else
	echo "Model path already set."
fi

if [[ $GAZEBO_PLUGIN_PATH != *"$TASC_GAZEBO_DIR/plugins/build"* ]]; then
	export GAZEBO_PLUGIN_PATH=$TASC_GAZEBO_DIR/plugins/build:$GAZEBO_PLUGIN_PATH
else
	echo "Plugin path already set."
fi

echo "Setup is done"
