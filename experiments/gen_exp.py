#!/usr/bin/python

'''
Generates files nesessary for given expiriments
'''

import sys
import getopt

def genWorld(N, header, footer, robot, plane_header, plane_footer, robot_joint, plugins):
	out = header

	for i in range(N):
		out += '\n'
		out += robot.replace('$ROBNUM$', '%d'%i).replace('$ROBXPOS$', '%f'%(-0.5 + i*1.0/N + 1.0/(2.0*N)))

	out += '\n' + plane_header

	for i in range(N):
		out += '\n'
		out += robot_joint.replace('$ROBNUM$', '%d'%i)

	out += '\n'
	out += plugins.replace('$NROBS', '%d'%N)
	out += plane_footer
	out += footer

	return out

def help():
	print 'Usage: gen_exp.py [-options]'
	print ' -n [--num_robs=]: number of robots in experiment (required)'
	print ' -e [--exp_name=]: name for the experiment'
	print ' -h [--help]: show this message'
	print ' -p [--plugins]: plugins to use (must provide templates)'

if __name__ == '__main__':
	N = 0
	wfp = '-empty-'
	plugin_templates = []
	world = 'AutoGenWorld'

	try:
		opts, args = getopt.getopt(sys.argv[1:], 'hn:e:p:', ['help', 'num_robs=', 'exp_name=', 'plugins='])
	except getopt.GetoptError as err:
		print 'Error reading opts...'
		print err.msg
		help()
		sys.exit(2)

	if len(opts) == 0:
		help()
		sys.exit()

	for opt, arg in opts:
		if opt in ['-h', '--help']:
			help()
			sys.exit()
		elif opt in ['-n', '--num_robs']:
			N = int(arg)
		elif opt in ['-e', '--exp_name']:
			wfp = '../worlds/%s.world'%arg
			world = arg
		elif opt in ['-p', '--plugins']:
			plugin_templates = arg.split(',')

	if wfp is '-empty-':
		wfp = '../worlds/MFRMFP%d.world'%N

	print 'generating experiment with %d robots...'%N
	print 'loading templates...'

	f_h = open('./.templates/header.tmp', 'r')
	f_f = open('./.templates/footer.tmp', 'r')
	f_r = open('./.templates/robot.tmp', 'r')
	f_ph = open('./.templates/planeHeader.tmp', 'r')
	f_pf = open('./.templates/planeFooter.tmp', 'r')
	f_rj = open('./.templates/robJoint.tmp', 'r')
	header = f_h.read().replace('$WORLD', world)
	footer = f_f.read()
	robot = f_r.read()
	plane_header = f_ph.read()
	plane_footer = f_pf.read()
	robot_joint = f_rj.read()
	f_h.close()
	f_f.close()
	f_r.close()
	f_ph.close()
	f_pf.close()
	f_rj.close()

	plugins = ''
	for i in range(len(plugin_templates)):
		f_p = open('./.templates/%s_plugin.tmp'%plugin_templates[i], 'r')
		plugins += f_p.read()
		f_p.close()

	print 'generating world file @ %s...'%wfp

	f_w = open(wfp, 'w')
	f_w.write(genWorld(N, header, footer, robot, plane_header, plane_footer, robot_joint, plugins))
	f_w.close()

	print 'Experiment ready!'
